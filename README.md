# DAAI-Clustering

Hi,

I did these experiments with the PyClustering Lib and the k-means clustering.
The PyClustering docu is pretty good and has examples for reprogramming (that's all I did in the experiments).
https://pyclustering.github.io/docs/0.8.2/html/index.html

I had a problem with numpy.  I went to numpy version 1.23.1. That might help if someone else is having problems. 

With k-means-increasing-sigma I wanted to find out when there is overlapping when I increase the variance.

With k-means-separate, I was interested in when there are overlaps in the normal distributions, 
how stable the k-means remains and how many points are clustered incorrectly.
Attention! The experiment assumes that at least one data point is clustered incorrectly in each normal distribution.
This often works well in the setup, but sometimes leads to an exception if a separated cluster is empty . 

For k-means-center-finding, I wanted to add how well the original centres can be found.
This works quite well. One of my tests had an average distance to the original centres of 0.048, max. distance of 0.082 and 
min. distance of 0.012. This varies because my data is regenerated each time, but it gives an impression of the order of magnitude.

In Clustering.pdf is my presentation from 24.04..

MfG
Benji
