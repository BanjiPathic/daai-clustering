from pyclustering.cluster.center_initializer import kmeans_plusplus_initializer
from pyclustering.cluster.kmeans import kmeans, kmeans_visualizer
import numpy

# Warning! this code is sometimes not stable, because the visualiser can't  work with empty clusters
# that happens sometimes in this setup, just retry

sigma = 0.5
n = 200

# generate the first 200 normal random points around (1,1) as expected value
mu = 1
sX = numpy.random.normal(mu, sigma, n).tolist() # generate the x components
sY = numpy.random.normal(mu, sigma, n).tolist() # generate the y components
sP1 = [[sX[i], sY[i]] for i in range(0,n-1)] # putting the components together in one list

# generate the second 200 normal random points around (2,2) as expected value
mu = 2
sX = numpy.random.normal(mu, sigma, n).tolist()
sY = numpy.random.normal(mu, sigma, n).tolist()
sP2 = [[sX[i], sY[i]] for i in range(0,n-1)]


# generate the second 200 normal random points around (1,3) as expected value
mu = 3
mu2 = 1
sX = numpy.random.normal(mu, sigma, n).tolist()
sY = numpy.random.normal(mu2, sigma, n).tolist()
sP3 = [[sX[i], sY[i]] for i in range(0,n-1)]

# putting the three point list into one list
sample = sP1 + sP2 + sP3

# some helper funktions to separate the clustering into the individual sample lists
def check_first(number):
    return number < len(sP1)

def check_second(number):
    return number >= len(sP1) and number < len(sP1)+len(sP2)

def check_third(number):
    return number >= len(sP1)+len(sP2)

def level_one_indices(number):
    return number - len(sP1)
def level_two_indices(number):
    return number - (len(sP1)+len(sP2))

def maxmax(cluster):
    current_max = -1
    for c in cluster:
        if c != [] and current_max < max(c):
            current_max = max(c)
    return current_max

def minmin(cluster):
    current_min = 201
    for c in cluster:
        if c != [] and current_min > min(c):
            current_min = min(c)
    return current_min

def has_empty(cluster):
    for c in cluster:
        if c == []:
            return True
    return False

# initialize initial centers using K-Means++ method
initial_centers = kmeans_plusplus_initializer(sample, 3).initialize()
# create instance of K-Means algorithm with prepared centers
kmeans_instance = kmeans(sample, initial_centers)
# run cluster analysis and obtain results
kmeans_instance.process()
clusters = kmeans_instance.get_clusters()
final_centers = kmeans_instance.get_centers()

# opens a window with the visualisation of the clustering
visualizer = kmeans_visualizer()
visualizer.show_clusters(sample, clusters, final_centers)

# separate the clusters
# the clustering is just a list of lists of ints, so a list of lists of indices
# this together with the list of points can be ploted
cluster1 = [list(filter(check_first, clusters[0])),list(filter(check_first, clusters[1])),list(filter(check_first, clusters[2]))]
cluster2 = [list(map(level_one_indices, list(filter(check_second, clusters[0])))), list(map(level_one_indices, list(filter(check_second, clusters[1])))), list(map(level_one_indices, list(filter(check_second, clusters[2]))))]
cluster3 = [list(map(level_two_indices, list(filter(check_third, clusters[0])))), list(map(level_two_indices, list(filter(check_third, clusters[1])))), list(map(level_two_indices, list(filter(check_third, clusters[2]))))]

# some debug infos for the consol, not importent, can be put in comment/deleted
print("cluster1 length check")
print(len(sP1)==len(cluster1[0])+len(cluster1[1])+len(cluster1[2]), ", ",len(sP1), ", ", len(cluster1[0])+len(cluster1[1])+len(cluster1[2]))
print("max: ", maxmax(cluster1), ", min: ", minmin(cluster1), ", has empty: ", has_empty(cluster1), "\n")
print("cluster2 length check")
print(len(sP2)==len(cluster2[0])+len(cluster2[1])+len(cluster2[2]), ", ",len(sP2), ", ", len(cluster2[0])+len(cluster2[1])+len(cluster2[2]))
print("max: ", maxmax(cluster2), ", min: ", minmin(cluster2), ", has empty: ", has_empty(cluster2), "\n")
print("cluster3 length check")
print(len(sP3)==len(cluster3[0])+len(cluster3[1])+len(cluster3[2]), ", ",len(sP3), ", ", len(cluster3[0])+len(cluster3[1])+len(cluster3[2]))
print("max: ", maxmax(cluster3), ", min: ", minmin(cluster3), ", has empty: ", has_empty(cluster3), "\n")
print("whole length")
print(len(sample), "\n" )


visualizer = kmeans_visualizer()
visualizer.show_clusters(sP1, cluster1, final_centers)

visualizer = kmeans_visualizer()
visualizer.show_clusters(sP2, cluster2, final_centers)


visualizer = kmeans_visualizer()
visualizer.show_clusters(sP3, cluster3, final_centers)