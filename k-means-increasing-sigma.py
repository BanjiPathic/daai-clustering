from pyclustering.cluster.center_initializer import kmeans_plusplus_initializer
from pyclustering.cluster.kmeans import kmeans, kmeans_visualizer
import numpy

for i in range(1,6):
    sigma = 0.1 * i # sigma in range 0.1 and 0.5
    n = 200

    # generate the first 200 normal random points around (1,1) as expected value
    mu = 1
    sX = numpy.random.normal(mu, sigma, n).tolist() # generate the x components
    sY = numpy.random.normal(mu, sigma, n).tolist() # generate the y components
    sP1 = [[sX[i], sY[i]] for i in range(0,n-1)] # putting the components together in one list

    # generate the second 200 normal random points around (2,2) as expected value
    mu = 2
    sX = numpy.random.normal(mu, sigma, n).tolist()
    sY = numpy.random.normal(mu, sigma, n).tolist()
    sP2 = [[sX[i], sY[i]] for i in range(0,n-1)]

    # generate the second 200 normal random points around (1,3) as expected value
    mu = 3
    mu2 = 1
    sX = numpy.random.normal(mu, sigma, n).tolist()
    sY = numpy.random.normal(mu2, sigma, n).tolist()
    sP3 = [[sX[i], sY[i]] for i in range(0,n-1)]

    # putting the three point list into one list
    sample = sP1 + sP2 + sP3

    # initialize initial centers using K-Means++ method
    initial_centers = kmeans_plusplus_initializer(sample, 3).initialize()
    # create instance of K-Means algorithm with prepared centers
    kmeans_instance = kmeans(sample, initial_centers)
    # run cluster analysis and obtain results
    kmeans_instance.process()
    clusters = kmeans_instance.get_clusters()
    final_centers = kmeans_instance.get_centers()

    # opens a window with the visualisation of the clustering
    visualizer = kmeans_visualizer()
    visualizer.show_clusters(sample, clusters, final_centers)