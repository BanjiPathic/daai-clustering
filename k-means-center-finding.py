import math

from pyclustering.cluster.center_initializer import kmeans_plusplus_initializer
from pyclustering.cluster.kmeans import kmeans, kmeans_visualizer
import numpy

runs = 10
sigma = 0.4
n = 200
normal_center_1 = [1, 1]
normal_center_2 = [2, 2]
normal_center_3 = [3, 1]


maxDist = 0
minDist = 100
summ = 0
ntel = 0

for i in range(0,runs):

    sX = numpy.random.normal(normal_center_1[0], sigma, n).tolist()
    sY = numpy.random.normal(normal_center_1[1], sigma, n).tolist()
    sP1 = [[sX[i], sY[i]] for i in range(0,n-1)]


    sX = numpy.random.normal(normal_center_2[0], sigma, n).tolist()
    sY = numpy.random.normal(normal_center_2[1], sigma, n).tolist()
    sP2 = [[sX[i], sY[i]] for i in range(0,n-1)]

    sX = numpy.random.normal(normal_center_3[0], sigma, n).tolist()
    sY = numpy.random.normal(normal_center_3[1], sigma, n).tolist()
    sP3 = [[sX[i], sY[i]] for i in range(0,n-1)]

    sample = sP1 + sP2 + sP3

    # initialize initial centers using K-Means++ method
    initial_centers = kmeans_plusplus_initializer(sample, 3).initialize()
    # create instance of K-Means algorithm with prepared centers
    kmeans_instance = kmeans(sample, initial_centers)
    # run cluster analysis and obtain results
    kmeans_instance.process()
    clusters = kmeans_instance.get_clusters()
    final_centers = kmeans_instance.get_centers()


    # in the following code is separatly searching the closest distance to the individual normal center
    # because the cluster centers arn't sorted, so the first cluster must not be the cluster of first normal distribution
    # the assumtion this code for finding the right works if the clustering is "quit good", which is the case here
    # this isn't a general solution

    disList = [0, 0, 0]
    for i in range(0,3):
        d = 100
        for c in final_centers:
            if math.dist(normal_center_1, c) < d:
                d = math.dist(normal_center_1, c)
        disList[i] = d
        if maxDist < d:
            maxDist = d
        if minDist > d:
            minDist = d
        summ += d
        ntel += 1

    print("dist 1: ", disList[0], ", dist 2: ", disList[1], ", dist 3: ", disList[2], )


av = summ/ntel
print("\nMaxDist: ", maxDist ,", MinDist: ", minDist , ", Average: ", av)
